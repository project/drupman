/**
* Some utitlity function for managing XUL trees.
*/
var XULTREE = {
	/**
	 * Add a new tree node into a tree or a treeitem.
	 * <treechildren> element will be added if needed.
	 */
	addChild : function(parentElement, childElement) {
		if (parentElement.localName != 'treeitem' &&  parentElement.localName != 'tree' )
			throw ('xultree.addChild : parentElement must be a "tree" or "treeitem"!');
		
		var treechildren = parentElement.firstChild;
		while (treechildren && treechildren.localName != "treechildren")
		  treechildren = treechildren.nextSibling;
		
		if(!isset(treechildren))
		   treechildren = parentElement.appendChild($E({ tag: "treechildren"  }));
		   
		parentElement.setAttribute("container", "true");
	  return treechildren.appendChild(childElement);
	},

	/**
	 * Add a new tree node into a tree.
	 * If a node with the same id existed, replace it if flag replace is set.
	 */
	addUniqueChild : function(parentElement, childElement) {
		oldChildren = parentElement.getElementsByAttribute('id', childElement.id); 
		if(oldChildren.length > 0)
			parentElement.replaceChild(oldChildren[0],childElement);
		else
		  this.addChild(parentElement, childElement);
		return childElement;
	},
	
	/**
	 * Clear content of a tree or a tree node while preserving its attributes.
	 */
	clearTreeContents : function(tree)	{
		if(tree.localName != 'tree' && tree.localName != 'treeitem')
			return;

		var treechildren = tree.firstChild;
		while (treechildren && treechildren.localName != "treechildren")
		  treechildren = treechildren.nextSibling;
		
		// Isset check is performed by removeAllChildNodes						
		removeAllChildNodes(treechildren);
	},
	
	/**
	 * Find and return <treechildren> element.
	 * If <treechildren> element is not existed, create it.
	 */ 
	getTreechildren : function(treeitem) {
		if (treeitem.localName != 'treeitem' &&  treeitem.localName != 'tree' )
			throw ('xultree.getTreechildren : parentElement must be a "tree" or "treeitem"!');
		
		var treechildren = treeitem.firstChild;
		while (treechildren && treechildren.localName != "treechildren")
		  treechildren = treechildren.nextSibling;
		
		if(!isset(treechildren))
		   treechildren = treeitem.appendChild($E({ tag: "treechildren"  }));
		   
		return treechildren;
	}
}
