/**
*
* Settings are saved in a hidden spacer located in mab.xul, using the persist attribute
*
* @FileName: settings.js
* @$LastChangedDate: 2004-05-13 18:49:36 +0200 (Thu, 13 May 2004) $
* @Author: Fabio Serra AKA Faser - faser@faser.net
* @Copyright: Fabio Serra
* @Licenze: MPL 1.1
*
*/

function getSettings() {
	//Get settings from spacer
	var op = window.opener;
	var searchType = op.document.getElementById('settings-spacer').getAttribute("search");
	var nrResult = op.document.getElementById('settings-spacer').getAttribute("nrResult");
		
	//Display settings in the dialog windows
	document.getElementById(searchType).setAttribute('selected','true');
	if(!nrResult) {
		nrResult = 10;
	}	
	var nrResultIndex = (nrResult/10) -1;
	document.getElementById("max-results").selectedIndex = nrResultIndex;
}

function setSettings() {
	var op = window.opener;
	
	//Get settings
	var searchType = document.getElementById('search').selectedItem.value;
	var nrResult = document.getElementById('max-results').selectedItem.value;
	
	//Save settings 
	op.document.getElementById('settings-spacer').setAttribute("search",searchType);
	op.document.getElementById('settings-spacer').setAttribute("nrResult",nrResult);
	
	window.close();
}