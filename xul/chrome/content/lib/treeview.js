
/**
 *  Notes on xulplanet :
 *	August 2, 2005, 4:56 am webmaster at swirldrop dot com
 *	The tree may not update the twisties correctly if you do not use a
 *	redrawing function, such as invalidateRow().
 *	See http://www.xulplanet.com/references/xpcomref/ifaces/nsITreeBoxObject.html
 *	for details on these functions.
 **/
const _TREEVIEW_ID = 0;
const _TREEVIEW_LEVEL = 1;
const _TREEVIEW_ISOPEN = 2;
const _TREEVIEW_TEMP_ID = 3;
	
function TreeView() {
	var childData = null;
	var visibleData = [];
	var columnIdMap = null;
	var DOMTree = null;
}

TreeView.prototype = {
	treeBox : null,
	
	setTree: function(treeBox)         { this.treeBox = treeBox; },
	
	isContainer: function(idx)         { return (this.itemAt(idx).hasChild == true); },
	isContainerOpen: function(idx)     { return this.visibleData[idx][_TREEVIEW_ISOPEN]; },
	isContainerEmpty: function(idx)    { /* return this.itemAt(idx).child.length == 0; */ },
	isSeparator: function(idx)         { return false; },
	isSorted: function()               { return false; },
	isEditable: function(idx, column)  { return false; },
	
	getParentIndex: function(idx) {
		for (var t = idx - 1; t >= 0 ; t--)
			if(this.getLevel(t) == (this.getLevel(idx)-1))
				return t;
		return t;
	},
	
	get rowCount()                     { return this.visibleData.length; },
	getCellText: function(idx, column) {
		if(this.columnIdMap && this.columnIdMap[column.id])
			return this.itemAt(idx)[this.columnIdMap[column.id]];
			
		return this.itemAt(idx)[column.id];
	},
	
	getLevel: function(idx) {
		return this.visibleData[idx][_TREEVIEW_LEVEL];
	},
	
	getImageSrc: function(idx, column) {
		if(this.itemAt(idx)['imageSrc'])
			return this.itemAt(idx)['imageSrc'];
	},
	
	hasNextSibling: function(idx, after) {
		var thisLevel = this.getLevel(idx);
		for (var t = idx + 1; t < this.visibleData.length; t++) {
			var nextLevel = this.getLevel(t)
			if (nextLevel == thisLevel) return true;
			else if (nextLevel < thisLevel) return false;
		}
	},

	toggleOpenState: function (idx) {
	    var item = this.visibleData[idx];
	    if (!this.isContainer(idx)) {
	        return;
	    }
	    
	    if (item[_TREEVIEW_ISOPEN]) { // isContainerOpen
	        item[_TREEVIEW_ISOPEN] = false;
	        var thisLevel = this.getLevel(idx);
	        var deletecount = 0;
	        for (var t = idx + 1; t < this.visibleData.length; t++) {
	            if (this.getLevel(t) > thisLevel) {
	                deletecount++;
	            } else {
	                break;
	            }
	        }
	        if (deletecount) {
	            this.visibleData.splice(idx + 1, deletecount);
	            this.treeBox.rowCountChanged(idx + 1, -deletecount);
	        }
	    } else {
	        item[_TREEVIEW_ISOPEN] = true;
	        var id = item[_TREEVIEW_ID];
	        var itemData = this.itemAt(idx);
	        toinsert = itemData.child;
	        if(!toinsert)
	        	toinsert = [];
	        
	        // Trying to load subnodes
	        if(toinsert.length <= 0)  {
	        	this.loadSubItems(idx);
	        }
			else {        
		        for (var i = 0; i < toinsert.length; i++) {
	            	this.visibleData.splice(idx + i + 1, 0, [toinsert[i], item[_TREEVIEW_LEVEL]+1, false]);
		        }
	        	this.treeBox.rowCountChanged(idx + 1, toinsert.length);
	        }
	    }
	},
	
	// Interface stubs	
	getCellValue: function(idx, column) {},
	getProgressMode : function(idx, column) {},
	cycleHeader: function(col, elem) {},
	selectionChanged: function() {},
	cycleCell: function(idx, column) {},
	performAction: function(action) {},
	performActionOnCell: function(action, index, column) {},
	getRowProperties: function(idx, props) {},
	getColumnProperties: function(column, element, props) {},
	getCellProperties: function(idx, column, props) {
	/**  TODO: Verify that will this code snippet will work in unprivilege mode.
		SAMPLE CODE
		if ((idx % 2) == 0){
		    var atom_serv=Components.classes["@mozilla.org/atom-service;1"].
		              getService(Components.interfaces.nsIAtomService);
		    props.AppendElement(atom_serv.getAtom("makeItBlue"));
		}
	*/
	},
	
	loadSubItems : function(item_idx) {}, /* Stub function */
  
	itemAt : function(idx)	{
		return this.childData[this.visibleData[idx][_TREEVIEW_ID]];
	},
	
	getRandomID : function() {
	    return new Date().getTime();
	},
	
	addTemporaryChild : function(item_idx, child_id, temp_id)
	{
		this.visibleData.splice(item_idx + 1, 0, [child_id, this.visibleData[item_idx][_TREEVIEW_LEVEL]+1, false]);
		this.visibleData[item_idx][_TREEVIEW_TEMP_ID] = temp_id;
		this.treeBox.rowCountChanged(item_idx + 1, 1);
	},
	 
	removeTemporaryChild : function(temp_id)
	{
		me = this;
		locateLoadingItem = function(temp_id) {
			for(var i=0;i < me.visibleData.length; i++)	{
				if(me.visibleData[i][_TREEVIEW_TEMP_ID] == temp_id)
					return i;
			}
			return -1;
		}
		
		var idx = locateLoadingItem(temp_id);
		if(idx >= 0) {
			var item = me.visibleData[idx];
			if(item[_TREEVIEW_ISOPEN]) {
				me.toggleOpenState(idx);
				if((typeof me.itemAt(idx).child == 'object') && (me.itemAt(idx).child.length > 0))
					me.toggleOpenState(idx);
			}
		}
	},
	
	getItemIndex : function(item_id) {
		for(var i=0;i < this.visibleData.length; i++) {
			if(this.visibleData[i][_TREEVIEW_ID] == item_id)
				return i;
		}
		return -1;
	},
	
	getItemAt : function(idx) {
		return this.itemAt(idx);
	},

	validateChildData : function() {
        for(var item_id in this.childData) {
            item = this.childData[item_id];
            for(i=0; i < item.child.length; i++)
            	if(!this.childData[item.child[i]])
            		throw ('Inconsistence ChildData : Item "' + item.child[i] + '" not defined!'); 
        }
	},
 	 
	/**
	 *  The twistys don't function, solution posted on the forum
	 *  http://xulplanet.com/forum/viewtopic.php?t=1285
	 */
	QueryInterface : function(aIID)
	{
		if (Components.interfaces.nsIClassInfo.equals(aIID) ||
			Components.interfaces.nsITreeView.equals(aIID) ||
			Components.interfaces.nsISupportsWeakReference.equals(aIID) ||
			Components.interfaces.nsISupports.equals(aIID))
		{
			return this;
		}
		throw 0x80004002; // Components.results.NS_NOINTERFACE;
	},
	     
	get flags() { return Components.interfaces.nsIClassInfo.DOM_OBJECT; },
	
	/* nsIClassInfo */
	getInterfaces: function getInterfaces(count) {
	    count.value = 4;
	    return [Components.interfaces.nsITreeView,
	            Components.interfaces.nsIClassInfo, 
	            Components.interfaces.nsISupportsWeakReference, 
	            Components.interfaces.nsISupports];
	},
	getHelperForLanguage: function getHelperForLanguage(language) { return null; },
	get contractID() { return null; },
	get classDescription() { return "TreeView"; },
	get classID() { return null; },
	get implementationLanguage() { return Components.interfaces.nsIProgrammimgLanguage.JAVASCRIPT; }	
};
