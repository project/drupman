/**
 * Quick shortcuts
 */
function $(element_id)
{
	return document.getElementById(element_id);
}

/* document.getBindingParent shortcut */
function $B(element)
{
	return document.getBindingParent(element);
}

/**
 * document.createElement convenience wrapper
 *
 * The data parameter is an object that must have the "tag" key, containing
 * a string with the tagname of the element to create.  It can optionally have
 * a "children" key which can be: a string, "data" object, or an array of "data"
 * objects to append to this element as children.  Any other key is taken as an
 * attribute to be applied to this tag.
 *
 * Release homepage:
 * http://www.arantius.com/article/dollar+e
 *
 * Available under an MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * @param {Object} data The data representing the element to create
 * @return {Element} The element created.
 */
 function $E(data) {
	/** EXAMPLE USAGE: *****************************
	var element=$E({
	    tag:'div',
	    className:'toolGroup',
	    id:'toolGroup_1',
	    children:{
	        tag:'div',
	        className:'roundBarTop',
	        children:[{
	            tag:'div',
	            className:'leftEdge'
	        },{
	            tag:'div',
	            className:'rightEdge'
	        },{
	            tag:'div',
	            className:'heading',
	            children:[{
	                tag:'a',
	                className:'collapser'
	            },
	                'Group Heading'
	            ]
	        }]
	    }
	});
	 
	** RESULTS : *******************************************
	<div id="toolGroup_1" class="toolGroup">
		<div class="roundBarTop">
			<div class="leftEdge"></div>
			<div class="rightEdge"></div>
			<div class="heading">
				<a class="collapser"></a>
				Group Heading
			</div>
		</div>
	</div>
	********************************************************/    
	var el;
	if(data.ownerDocument == document)
		return data;
		
	if('object' != typeof data) {
		el = document.createTextNode(""+data);
	} else {
		
		if((typeof data.tag) == 'undefined')	{
			Logger.warning('$E : Elements must have a tag name ' + (data));
			return false;
		}
			
		//create the element
		el=document.createElement(data.tag);
		delete(data.tag);

		//append the children
		if ('undefined' != typeof data.children) {
			if ('string' == typeof data.children ||	'undefined' == typeof data.children.length) {
				//strings and single elements
				el.appendChild($E(data.children));
			} else {
				//arrays of elements
				for (var i=0, child=null; 'undefined'!=typeof (child=data.children[i]); i++) {
					el.appendChild($E(child));
				}
			}
			delete(data.children);
		}

		//any other data is attributes
		for (attr in data) {
			el.setAttribute(attr,data[attr]);
		}
	}

	return el;
}

// http://simon.incutio.com/archive/2004/05/26/addLoadEvent
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function toggleHidden(el, hidden) {
    if (!isset(hidden)) {
        hidden = !el.hidden;
    }
    el.hidden = hidden;
}

function toggleCollapsed(el, collapsed) {
    if (!isset(collapsed)) {
        collapsed = !el.collapsed;
    }
    el.collapsed = collapsed;
}

function toggleGroupboxCollapsed(el, collapsed) {
    if (!isset(collapsed)) {
        collapsed = !Boolean(el.getAttribute("groupboxcollapsed")=='true');
    }
    el.setAttribute("groupboxcollapsed", collapsed);
    for (var i = 0; i < el.childNodes.length; i++) {
        if(el.childNodes[i].localName != "caption") {
            toggleCollapsed(el.childNodes[i], collapsed);
        }
    }
}

function disableRadiogroup(rg, bool) {
    for (var i = 0; i < rg.childNodes.length; i++) {
        if (rg.childNodes[i].localName == "radio") {
            rg.childNodes[i].setAttribute("disabled", bool ? "true" : "false");
        }
    }
}

function removeDOMNode(obj) {
	if(isset(obj.parentNode))
		return obj.parentNode.removeChild(obj);
	return obj;
}

function removeAllChildNodes(element) {
	if(isset(element))
	while (element.hasChildNodes()) {
	  element.removeChild(element.lastChild);
	}
}

function setWaitingState(tree_node)
{
	if(tree_node.localName != 'treeitem')
		return;
		
	var loading_item = $E({
		tag : 'treeitem',
		id  : getRandomID(),
		children : {
			tag : 'treerow',
			children : {
				tag : 'treecell',
				properties : 'loading',
				label : ' Loading ... '
			}
		}
	});
	XULTREE.addChild(tree_node, loading_item);
	return loading_item;
}

function getRandomID() {
	return CONFIG.RANDOM_PREFIX + (new Date().getTime());
}

function isTypeOf(stringID,type)
{
	var typesMap = {
		'vocabulary' : CONFIG.VOCABULARY_PREFIX,
		'nodetype' 	 : CONFIG.NODETYPE_PREFIX,
		'term' 		 : CONFIG.TERM_PREFIX,
		'comment' 	 : CONFIG.COMMENT_PREFIX,
		'node' 		 : CONFIG.NODE_PREFIX,
		'random'     : CONFIG.RANDOM_PREFIX,
	};
	
	return stringID.lastIndexOf(typesMap[type]) == 0;
}

function setTitle(node, text)
{
		switch(node.localName)
		{
			case 'label':
			case 'textbox':
				node.setAttribute("value",text);
				break;
				
			case 'groupbox':
				var caption = node.getElementsByTagName('caption');
				if(caption.length > 0)
					caption = caption[0];
				else
				{
					caption = $E({tag : 'caption',
						label : text,
						onclick : 'toggleGroupboxCollapsed(this.parentNode)'
						});
					node.appendChild(caption);
				}
				return caption;
    		break;
    		
			default:
				node.setAttribute('label', text);
				break;
		}
}
			
function isElementChild(key) {
	return (key.toString().indexOf('#',0) != 0);
}
