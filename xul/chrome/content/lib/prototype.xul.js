/*  Prototype JavaScript framework, version 1.5.0_pre1
 *  (c) 2005 Sam Stephenson <sam@conio.net>
 *
 *  Prototype is freely distributable under the terms of an MIT-style license.
 *  For details, see the Prototype web site: http://prototype.conio.net/
 *
/*--------------------------------------------------------------------------*/

/**
 * Modified version for working with XUL.
 */
function $(element_id)
{
	return document.getElementById(element_id);
}
/* document.getBindingParent shortcut */
function $B(element)
{
	return document.getBindingParent(element);
}

/**
 * document.createElement convenience wrapper
 *
 * The data parameter is an object that must have the "tag" key, containing
 * a string with the tagname of the element to create.  It can optionally have
 * a "children" key which can be: a string, "data" object, or an array of "data"
 * objects to append to this element as children.  Any other key is taken as an
 * attribute to be applied to this tag.
 *
 * Release homepage:
 * http://www.arantius.com/article/dollar+e
 *
 * Available under an MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * @param {Object} data The data representing the element to create
 * @return {Element} The element created.
 */
 function $E(data) {
	/** EXAMPLE USAGE: *****************************
	var element=$E({
	    tag:'div',
	    className:'toolGroup',
	    id:'toolGroup_1',
	    children:{
	        tag:'div',
	        className:'roundBarTop',
	        children:[{
	            tag:'div',
	            className:'leftEdge'
	        },{
	            tag:'div',
	            className:'rightEdge'
	        },{
	            tag:'div',
	            className:'heading',
	            children:[{
	                tag:'a',
	                className:'collapser'
	            },
	                'Group Heading'
	            ]
	        }]
	    }
	});
	 
	** RESULTS : *******************************************
	<div id="toolGroup_1" class="toolGroup">
		<div class="roundBarTop">
			<div class="leftEdge"></div>
			<div class="rightEdge"></div>
			<div class="heading">
				<a class="collapser"></a>
				Group Heading
			</div>
		</div>
	</div>
	********************************************************/    
	var el;
	if(data.ownerDocument == document)
		return data;
		
	if('object' != typeof data) {
		el = document.createTextNode(""+data);
	} else {
		
		if((typeof data.tag) == 'undefined')	{
			Logger.warning('$E : Elements must have a tag name ' + (data));
			return false;
		}
			
		//create the element
		el=document.createElement(data.tag);
		delete(data.tag);

		//append the children
		if ('undefined' != typeof data.children) {
			if ('string' == typeof data.children ||	'undefined' == typeof data.children.length) {
				//strings and single elements
				el.appendChild($E(data.children));
			} else {
				//arrays of elements
				for (var i=0, child=null; 'undefined'!=typeof (child=data.children[i]); i++) {
					el.appendChild($E(child));
				}
			}
			delete(data.children);
		}

		//any other data is attributes
		for (attr in data) {
			el.setAttribute(attr,data[attr]);
		}
	}

	return el;
}
