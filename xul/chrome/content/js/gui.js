//-------- GUI Messages --------//

var messageTimerID = 0;
var messageTimerRemain = 0;

function UpdateMessageTimer() {
   	
    if (messageTimerRemain <= 0) {
        clearMessage();
    } else {
        $("messageTimer").value = messageTimerRemain + ""; 
        messageTimerRemain--;
        
	   	clearTimeout(messageTimerID);
    	messageTimerID = setTimeout("UpdateMessageTimer()", 1000);
    }
}

//-->
function setMessage(message) {
    $("messageBar").setAttribute("hidden", "false");
    $("messageText").value = message;
    
    Logger.info("Status Message : " + message);
    messageTimerRemain = CONFIG.MESSAGE_DISPLAY_TIME;
    messageTimerID = setTimeout("UpdateMessageTimer()", 1000);
}
function clearMessage() {
    $("messageText").value = "";
    $("messageBar").setAttribute("hidden", "true");
    clearTimeout(messageTimerID);
}

// Date formatting function
// http://www.mikezilla.com/exp0015.html
function formatDate(vDate, vFormat) {
    var vDay = addZero(vDate.getDate());
    var vMonth = addZero(vDate.getMonth() + 1);
    var vYearLong = addZero(vDate.getFullYear());
    var vYearShort = addZero(vDate.getFullYear().toString().substring(3, 4));
    var vYear = (vFormat.indexOf("yyyy") > -1 ? vYearLong : vYearShort);
    var vHour = addZero(vDate.getHours());
    var vMinute = addZero(vDate.getMinutes());
    var vSecond = addZero(vDate.getSeconds());
    var vDateString = vFormat.replace(/dd/g, vDay).replace(/MM/g, vMonth).replace(/y{1,4}/g, vYear);
    vDateString = vDateString.replace(/hh/g, vHour).replace(/mm/g, vMinute).replace(/ss/g, vSecond);
    
    return vDateString;
}

function implode(str, arr) {
    var ret = "";
    if (typeof arr == "undefined" || typeof arr.length == "undefined" || arr.length <= 0) {
        ret = "";
    } else {
        if (arr.length == 1) {
            ret = arr[0] + "";
        } else {
            for (var i = 0; i < arr.length - 1; i++) {
                ret += arr[i] + str;
            }
            ret += arr[i];
        }
    }
    return ret;
}

function isset(obj) {
    return (typeof obj != "undefined") && (obj != null);
}

function isset_string(obj) {
    return (typeof obj != "undefined") && (obj != null) && (typeof obj != 'string' || obj != '');
}

function progressEnd() {
    var bar = $("progress");
    bar.mode = "determined";
    bar.value = "0%";
    bar.hidden = true;
}

function collapsebar(control) {
    var state = control.getAttribute("state");
    alert("colapssing");
    switch (state) {
      case "0":
        /* expand */
        $("panelView").setAttribute("hidden", "true");
        $("panelView").setAttribute("height", "0px");
        $("panelPosts").setAttribute("hidden", "false");
        control.setAttribute("state", "1");
        break;
      case "1":
        /* collapse */
        $("panelPosts").setAttribute("hidden", "true");
        $("panelPosts").setAttribute("height", "0px");
        $("panelView").setAttribute("hidden", "false");
        control.setAttribute("state", "2");
        break;
      case "2":
        /* normal */
        $("panelPosts").setAttribute("flex", "1");
        $("panelView").setAttribute("flex", "2");
        $("panelPosts").setAttribute("hidden", "false");
        $("panelView").setAttribute("hidden", "false");
        control.setAttribute("state", "0");
        break;
    }
}

function panelcollapse(state) {
    switch (state) {
      case 0:
        /* normal */
        $("panelPosts").setAttribute("flex", "1");
        $("panelView").setAttribute("flex", "2");
        $("panelPosts").setAttribute("collapse", "false");
        $("panelView").setAttribute("collapse", "false");
        collapsestate = 0;
        break;
      case 1:
        /* expand */
        $("panelPosts").setAttribute("flex", "0");
        $("panelPosts").setAttribute(panelstate == 0 ? "height" : "width", "0px");
        $("panelView").setAttribute("flex", "1");
        $("panelPosts").setAttribute("collapse", "true");
        $("panelView").setAttribute("collapse", "false");
        collapsestate = 1;
        break;
      case 2:
        /* collapse */
        $("panelView").setAttribute("flex", "0");
        $("panelView").setAttribute(panelstate == 0 ? "height" : "width", "0px");
        $("panelPosts").setAttribute("flex", "1");
        $("panelView").setAttribute("collapse", "true");
        $("panelPosts").setAttribute("collapse", "false");
        collapsestate = 2;
        break;
    }
}


function panelViewmode(state) {
    switch (state) {
      case 0:
        /* horizontal */
        $("workbench").setAttribute("orient", "vertical");
        $("panelSplit").setAttribute("orient", "vertical");
        panelstate = 0;
        break;
      case 1:
        /* vertical */
        $("workbench").setAttribute("orient", "horizontal");
        $("panelSplit").setAttribute("orient", "horizontal");
        panelstate = 1;
        break;
    }
    panelcollapse(collapsestate);
}

function flipHeaderBar() {
    var bar = $("mainBar");
    bar.hidden = !bar.hidden;
    var drupmanControl = $('drupmanControl');
    if(bar.hidden)
    {
    	$('mainMenubar').appendChild(drupmanControl);
    	$('flipHeaderBarCmd').setAttribute('checked','true');
    }
    else
    {
    	$("drupmanControlContainer").appendChild(drupmanControl);
    	$('flipHeaderBarCmd').removeAttribute('checked');
    }
}

function switchMainView(panelID)
{
	var panel = $(panelID);
	
	// Switching main view panel
	$('mainView').selectedPanel = panel;
	$('mainViewContainer').setAttribute('label', panel.getAttribute('label'));
	
	// Switching default toolbar
	var toolbar = $(panel.getAttribute('attachedToolbar'));
	if(isset(toolbar))
	{
		$('mainToolbar').selectedPanel = toolbar;
		$('mainToolbar').hidden = false;
	}
	else
		$('mainToolbar').hidden = true;
		
	// Switching sidebar
	var sidebar = $(panel.getAttribute('attachedSidebar'));
	if(!isset(sidebar)) sidebar = $('NavigationTree'); 
	$('sidebarPanelContainer').selectedPanel = sidebar;
}

function showhelp() {
//  window.open(URL.help);
//  window.getBrowser().addTab(URL.help);
    $("viewarea").setAttribute("src", URL.help);
}

function clearcookies() {
    document.cookie = "";
    webMethod(URL.clearcookies, null, aftercookies);
}

function aftercookies(dummy) {
    document.cookie = "";
    alert("Cookies have been cleared");
}

function gotoDrupal() {
    window.open("http://www.drupal.org");
}

function updateHeaderStatus()
{
	var site = DRUPMAN.getActiveSite();
	if(isset(site))
		switch(site.getStatus())
		{
			case 'available':
				$('lblSessionId').value = "Not logged in";
				$('tbLogout').hidden = false;
				$('tbLogout').setAttribute("onclick","var s = DRUPMAN.getActiveSite(); if(isset(s)) s.login();");
				$('tbLogout').setAttribute("tooltiptext","Login");
				break;
				
			case 'logged_in':	
				$('lblSessionId').value = "Logged in as " + site.session.userData.name;
				$('tbLogout').hidden = false;
				$('tbLogout').setAttribute("onclick","var s = DRUPMAN.getActiveSite(); if(isset(s)) s.logout();");
				$('tbLogout').setAttribute("tooltiptext","Log out");
				break;
				
			default:
				$('lblSessionId').value = "Unavailable";
				$('tbLogout').hidden = true;
				break;
		}
}

/**
* Function : dump()
* Arguments: The data - array,hash(associative array),object
*    The level - OPTIONAL
* Returns  : The textual representation of the array.
* This function was inspired by the print_r function of PHP.
* This will accept some data as the argument and return a
* text that will be a more readable version of the
* array/hash/object that is given.
*/
function dumpText(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	if(level > 10) return;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
  		var value = arr[item];
 
  		if(typeof(value) == 'object' && isset(value)) { //If it is an array,
   			dumped_text += level_padding + item + " : {\n";
   			dumped_text += dumpText(value,level+1);
   			dumped_text += level_padding + "}\n";
  		} else if(typeof value != 'function') {
   			dumped_text += level_padding +item + " : " + value + "\n"; // + "\" [" + (typeof value) + "],\n";
  		}
 		}
	} else if(typeof arr != 'function')	{ //Stings/Chars/Numbers etc.
  	dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}
