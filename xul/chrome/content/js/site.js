DRUPMAN.Session = function(sess) {
	this.sessionID =  sess[0];
	this.userData = sess[1];
}

/**
 * Create an instance of a Drupal website
 * Site Url is the url to the root of the web site without the slash.
 */
DRUPMAN.Site = function(name, siteUrl, user, pass)	{
	this.sitename = name;
	this.username = user;
	this.password = pass;
	this.service = null;
	this.session = null;
	this.sid = getRandomID();
	this.url = siteUrl;
	this.xmlrpcURL = siteUrl + "/xmlrpc.php";;
	this.status = 'unavailable';
	
	this.queryService();
}

DRUPMAN.Site.prototype.queryService = function()
{
	// Create a new service with a single method to prevent introspection.
	var	service = new XMLRPC.ServiceProxy(this.xmlrpcURL, ["system.listMethods","system.methodHelp", "system.methodSignature"]);
	var _self = this;
			
	function processListMethods(results, exc)
	{
		if(isset(exc))
		{
			_self.service = null;
		  _self.updateSiteStatus();
	    Logger.info("Cannot query services of site '" + this.sitename + "'. Maybe security restriction.\nException :",exc);
			return false;
		}
		service._addMethodNames(results);
				 
		_self.service = service;
		_self.updateSiteStatus();
	  _self.checkLogin();
		return true;
	}
	
  service.system.listMethods(processListMethods);
}
		
DRUPMAN.Site.prototype.getStatus = function() {
	return this.status;
}
		
DRUPMAN.Site.prototype.updateSiteStatus = function() {
	var newStatus = 'unavailable';
	if(!isset(this.service))
		newStatus = 'unavailable'; // "Not Available";
	else if(!isset(this.session))
		newStatus = 'available'; // "Not Logged In";
	else
		newStatus = 'logged_in'; // "Logged In";
				
	if(newStatus != this.status)
	{
		this.status = newStatus;
		DRUPMAN.emitEvent(this, "siteStatusChanged", this.status);
	}
}
		
		
DRUPMAN.Site.prototype.login = function() {
  var _self = this;
	function processLogin(results, exception)
	{
		  if(isset(exception))
		  {
		  	Logger.info("Exception caught! Login failed.", exception);
      setMessage(_self.username + " failed logging in site '" + _self.sitename + "'. See log for details.");
		    _self.updateSiteStatus();
		  	return false;
		  }
		  
    _self.session = new DRUPMAN.Session(results);
		_self.updateSiteStatus();
    setMessage(_self.username + " successfully logged in site '" + _self.sitename + "'");
    return true;
	}

	this.service.system.login(this.username, this.password, processLogin);
}
		
DRUPMAN.Site.prototype.checkLogin = function() {
  var _self = this;
	function processCheckLogin(results, exception)
	{
	  if(isset(exception))
	  {
	  	Logger.info("Attempt to restore previous session of site " + _self.sitename + " failed.", exception);
	  	return false;
	  }
	  
		_self.session = new DRUPMAN.Session(results);
		_self.updateSiteStatus();
		setMessage("Restored previous session of user '" + _self.username + "' with site '" + _self.sitename + "'");
		return true;
	}
	
	this.service.system.checkLogin(processCheckLogin);		
}
		
DRUPMAN.Site.prototype.logout = function() {
	var _self = this;
	function processLogout(results, exception) {
		_self.session = null;
		_self.updateSiteStatus();
		setMessage(_self.username + " successfully logged OFF drupal site '" + _self.sitename + "'");
	}
			
	this.service.system.logout(processLogout);		
}

	
