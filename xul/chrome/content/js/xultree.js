/**
* Class for managing sites, each site is an instance of this class.
*/
Module("xultree", "0.0.1", function(mod) {
		mod.addChild = function(parentElement, childElement) {
			if (parentElement.localName != 'treeitem' &&  parentElement.localName != 'tree' )
				throw ('xultree.addChild : parentElement must be a "tree" or "treeitem"!');
			
			var treechildren = parentElement.firstChild;
			while (treechildren && treechildren.localName != "treechildren")
			  treechildren = treechildren.nextSibling;
			
			if(!isset(treechildren))
			   treechildren = parentElement.appendChild($E({ tag: "treechildren"  }));
			   
		  treechildren.appendChild(childElement);
			parentElement.setAttribute("container", "true");
		}

		mod.addUniqueChild = function(parentElement, childElement) {
			oldChildren = parentElement.getElementsByAttribute('id', childElement.id); 
			if(oldChildren.length > 0)
				parentElement.replaceChild(oldChildren[0],childElement);
			else
			  this.addChild(parentElement, childElement);
		}
});
