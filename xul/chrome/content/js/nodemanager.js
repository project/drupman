
function addNodeEditorTab(nodeData, site)
{
	var tabbox = $('nodeManagerTabbox');
	var tabs = tabbox.firstChild;
	var tabpanels = tabbox.lastChild;

	if(!isset(tabbox.editingNodes))
		tabbox.editingNodes = [];
	
	if(isset(tabbox.editingNodes[nodeData.nid]))
	{
		tabbox.selectedTab = tabbox.editingNodes[nodeData.nid];
		return;
	}
	
	var tab = $E({ tag : 'tab',
		prefix : nodeData.type,
		label : nodeData.title,
		tooltiptext : nodeData.title
	});
	
	tab.minWidth = 100;
	tab.maxWidth = 250;
	tab.setAttribute("flex",20);
	tab.nid = nodeData.nid;
	
	var tabpanel = $E({
		  tag : 'tabpanel',
		  id : 'nodeditor'+nodeData.id,
			orient : 'vertical',
			class : 'node-editor'
		});
		
	tab.linkedPanel = tabpanel;
	tabpanel.linkedTab = tab;
	
	tabs.appendChild(tab);
	tabpanels.appendChild(tabpanel);
	
	tabpanel.loadNode(nodeData,site);
	tabbox.editingNodes[nodeData.nid] = tab;
}

function removeNodeEditorTab(event)
{
	var tabbox = $('nodeManagerTabbox');
	var tabs = tabbox.firstChild;
	var tabpanels = tabbox.lastChild;

	var tab = event.target;
	var tabpanel = tab.linkedPanel;
	
	// Removing tab is selected tab,
	// select the next one, or previous one
	if(tab == tabbox.selectedTab)
	{
		var toselect;
		if(isset(tab.nextSibling))
			toselect = tab.nextSibling;
		else
			toselect = tab.previousSibling;
			
		tabbox.selectedTab = toselect;
	}
	
	delete tabbox.editingNodes[tab.nid];		
	tabs.removeChild(tab);
	tabpanels.removeChild(tabpanel);
	
}

function initNodesManager()
{
	var tabbox = $('nodeManagerTabbox');
	tabbox.addEventListener('closeButtonClicked',removeNodeEditorTab,true);
}

addLoadEvent(initNodesManager);