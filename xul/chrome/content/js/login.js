//---------- LOGIN/OUT ----------------//
var loginHttpRequest = undefined;

function processLogin(results, exception) {

	  resetLoginForm();
	  
	  if(isset(exception))
	  {
	  	setMessage(exception +"");
	  	return false;
	  }
	  
	  /*
    if(response.status != 0)
    {
        loggedOut();
        setMessage("XMLRPC : " + response.message);
        return false;
    }
    parseResult(response);
    
    response = parseXMLRpcResponse(response.content);
    
    if(response.faultCode || !response[0])
    {
        loggedOut();
        if(response.faultCode < 0)
        	setMessage("Server returned an error : " + response.faultString);
        else if(response.faultCode != 4)
        	setMessage("Cannot log in. Possibly wrong username and/or password!");
        return false;
    }
    
    var results = response[0]; // Get first element of array
    */
    
  SESSION.ID =  results[0];
  SESSION.USERNAME = results[1].name;
    
//	setMessage("Have a good day, " + SESSION.USERNAME + "!");
	
	loggedIn();
	loadContents();
	return true;
}

function processCheckLogin(results, exception)
{
	if(!processLogin(results, exception))
		$('mainDeck').selectedIndex = 0;
	$('loginLoadingBox').hidden = true;
	$('loginGroupbox').hidden = false;
}

function abortLogin()
{
	if(isset(loginHttpRequest))
	{
		loginHttpRequest.abort();
		resetLoginForm();
	}
}

function resetLoginForm()
{
	loginHttpRequest = null;
	$("loginProgress").mode = "determined";    
	$("loginProgress").hidden = true;
	$("loginUsername").disabled = false;
	$("loginPassword").disabled = false;
	$("loginProfile").disabled = false;
	$("loginButton").label = "Login";
}

function doLogin()
{
	if(isset(loginHttpRequest))
	{
		abortLogin();
		return false;
	}
	
	var username = $('loginUsername').value;
	var password = $('loginPassword').value;
	
	if(CONFIG.DRUPMAN_RUN != 'remote')
		SESSION.URL = $('loginProfile').value;
		
	/* Processing function */
	
	$("loginProgress").mode = "undetermined";
	$("loginProgress").hidden = false;
	$("loginUsername").disabled = true;
	$("loginPassword").disabled = true;
	$("loginProfile").disabled = true;
	$("loginButton").label = "Abort";
	
	loginHttpRequest = service.system.login(username, password, processLogin);
}

function doCheckLogin()
{
	/* Preparing for a function call */
	service.system.checkLogin(processCheckLogin);
}

function doLogout()
{
	// var xmlrpc_logout = makeXMLRpcCall("system.logout",[]);
	// xmlrpc_logout.sendRequest(SESSION.URL, loggedOut);
	service.system.logout(loggedOut);
	setMessage("Logging out of the system ...");
}

//-------- Progress bar --------//
function loggedIn()
{
//	$("lbLoginId").value = "ID: " + SESSION.USERNAME;
	$("mainDeck").selectedIndex = 1;
}

function loggedOut()
{
	$("lbLoginId").value = "Not logged in.";
	$("mainDeck").selectedIndex = 0;
	clearMessage();
}

