/**
* DRUPMAN namespace
*/
var DRUPMAN = {
	SiteList : {},
	activeSID : undefined,
	
	addSite : function(name, url, user, pass) {
	  var site = new DRUPMAN.Site(name, url, user, pass);
		this.SiteList[site.sid] = site;
		this.emitEvent(site, "siteListChanged", "add");
		return site;
	},

	
	// Remove a site from the site list.
	removeSite : function(site) {
		var id;
		
		// Trying to detect whether parameter passed in is a site object or just an id string
		if(typeof site == 'object' && isset(site.sid) && isset(this.SiteList[site.sid]))
			id = site.sid;
		else if(isset(this.SiteList[site]))
			id = site;
		
		// Removing
		var site = this.SiteList[id];
		delete this.SiteList[id];
		this.emitEvent(site, "siteListChanged", "remove");
		return true;
	},
	
	switchActiveSite : function(sid)
	{
		if(this.activeSID != sid)
		{
			var site = this.SiteList[sid];
			if(isset(site))
			{
				this.activeSID = sid;
				this.emitEvent(site,"activeSiteChanged", site.getStatus());
			}
		}
	},
	
	getActiveSite : function() {
		if(isset(this.SiteList[this.activeSID]))
			return this.SiteList[this.activeSID];
			
		return null;
	},
	
	emitEvent : function(site, eventName, action)	{
		var evt = document.createEvent("Events");
		evt.initEvent(eventName, true, true);
		evt.targetSite = site;
		evt.action = action;
		window.dispatchEvent(evt);
	}
}

/**
 * inheritance
 * 
 * @author Kevin Lindsey
 * @version 1.0
 * 
 * copyright 2006, Kevin Lindsey
 * http://www.kevlindev.com/tutorials/javascript/inheritance/
 */

/**
 * A function used to extend one class with another
 * 
 * @param {Object} subClass
 * 		The inheriting class, or subclass
 * @param {Object} baseClass
 * 		The class from which to inherit
 */
DRUPMAN.extend = function(subClass, baseClass) {
   function inheritance() {}
   inheritance.prototype = baseClass.prototype;

   subClass.prototype = new inheritance();
   subClass.prototype.constructor = subClass;
   subClass.baseConstructor = baseClass;
   subClass.superClass = baseClass.prototype;
}
