/**
*  NOT USED YET
*/
Module("preferences", "0.0.1", function(mod) {
	mod.PrefTreeItem = Class(NAVTREE.NavtreeItem, function(publ, priv, supr)	{
		publ.name;
		publ.label;
		publ.configPath;
		publ.site;
		
		publ.createDOMNode = function(nodeLabel, props) {
			if(!isset(this.DOMNode))
			{
				this.DOMNode = $E({
					tag: 'treeitem',
					children : [{
						tag: 'treerow',
						children : [{
							tag: 'treecell',
							label : nodeLabel,
							properties : props
						},{
							tag: 'treecell',
						},{
							tag: 'treecell',
						},{
							tag: 'treecell'
						},{
							tag: 'treecell',
						},{
							tag: 'treecell',
						}]
					}]
				});
			}
			
			this.DOMNode.navtreeObject = this;
			return this.DOMNode;
		}
		
		publ.loadSubItems = function()
		{
			function processTreeItems(results, e)
			{
				if(isset(e))
					return;
					
				_self.loadConfigPage(results, configPage);
			}
			
			this.site.service.remoteconfig.traverseConfigTree(config_path, false, processTreeItems);
		}
	});
	
	/** Module **********************************************/
	mod.Module = Class(mod.PrefTreeItem, function(publ, priv, supr)	{
	  publ.module;
	  
		publ.__init__ = function(s, m, lbl)
		{
			this.site = site;
			this.module = m;
			this.label = lbl;
			this.createDOMNode();
			this.populateNodeData();
		}
		
		publ.loadSubItems = function()
		{
			var _self = this;
			function processTermSubtree(results, e)
			{
				for(var i=0; i < results.length; i++)	{
				  var term = new mod.Term(_self.site, results[i]);
			   	XULTREE.addUniqueChild(_self.DOMNode, term.DOMNode);
				}
				_self.removeWaitingState();
			}
			
			this.setWaitingState();
			this.site.service.taxonomy.getTermSubtree(parseInt(this.vocabulary.vid), 0, 1, processTermSubtree);
		}
		
		publ.populateNodeData = function(data)
		{
			var treerow = this.DOMNode.firstChild;
			treerow.childNodes[1].setAttribute('label',this.label);
			treerow.childNodes[2].setAttribute('label',this.configPath);
			data['#description'] && treerow.childNodes[3].setAttribute('label', this.description);
			data['#name'] && treerow.childNodes[4].setAttribute('label', data['#name']);
		}
	});
	
	/** Form/Config Item **********************************************/
	mod.Item = Class(mod.PrefTreeItem, function(publ, priv, supr)	{
	  publ.term;
		publ.__init__ = function(site, tm)
		{
			this.term = tm;
			this.site = site;
			this.createDOMNode(tm.name, "term");
		}
		
		publ.loadSubItems = function()
		{
			var _self = this;
			function processTermSubtree(results)
			{
				var count = 0;
				for(var i=0; i < results.length; i++)	{
				  var term = new mod.Term(_self.site, results[i]);
			   	XULTREE.addUniqueChild(_self.DOMNode, term.DOMNode);
			   	count++;
				}
				
				if(count == 0)
		  		_self.DOMNode.setAttribute('container','false');
				
				_self.removeWaitingState();
			}
			this.setWaitingState();
			this.site.service.taxonomy.getTermSubtree(parseInt(this.term.vid), parseInt(this.term.tid), 1, processTermSubtree);
		}
	});
	
	/** NodeTypesList **********************************************/
	mod.Page = Class(NAVTREE.NavtreeItem, function(publ, priv, supr)	{
		publ.__init__ = function(site) {
			this.site = site;
		  this.createDOMNode("Node Types",	"nodetypes");
		}
		
		publ.loadSubItems = function()
		{
		  var _self = this;
			function processNodeTypes(results)
			{
				for(var i=0; i < results.length; i++)	{
				   var term = new mod.NodeType(_self.site, results[i]);
		   		 XULTREE.addChild(_self.DOMNode, term.DOMNode);
				}
		  	_self.removeWaitingState();
			}
			
			this.setWaitingState();
			this.site.service.node.getNodeTypes(processNodeTypes);
		}
	});

	/** Site **********************************************/
	mod.Site = Class(mod.PrefTreeItem, function(publ, priv, supr)	{
		publ.__init__ = function(site) {
			this.site = site;
			this.label = site.name;
			this.configPath = "";
			this.description = site.url;
		  this.createDOMNode(site.sitename,	"site");
		}
		
		publ.loadSubItems = function()
		{
			var nodetypes = new mod.NodeTypesList(this.site);
			var vocabularies = new mod.VocabulariesList(this.site);
  		XULTREE.addChild(this.DOMNode, nodetypes.DOMNode);
  		XULTREE.addChild(this.DOMNode, vocabularies.DOMNode);
		}
		
		publ.populateNodeData = function()
		{
			var treerow = this.DOMNode.firstChild;
			treerow.childNodes[1].setAttribute('label',this.label);
			treerow.childNodes[2].setAttribute('label',this.configPath);
			treerow.childNodes[3].setAttribute('label', this.description);
		}
		
	});
	
});
