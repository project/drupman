var isloaded = false;
function init()
{

}
	
function querySite()
{
	clearTreeContents($('methodTree'));
	
}

function getRandomId()
{
}

function goHome()
{
	$("mainDeck").selectedIndex = 0;
}

function viewLogs()
{
	$("mainDeck").selectedIndex = 1;
}

//---------- LOGIN/OUT ----------------//
var loginHttpRequest = undefined;

function processLogin(response) {

	resetLoginForm();
    if(response.status != 0)
    {
        loggedOut();
        setMessage("XMLRPC : " + response.message);
        return false;
    }
    
    response = parseXMLRpcResponse(response.content);
    
    if(response.faultCode || !response[0])
    {
        loggedOut();
        if(response.faultCode != 4)
        	setMessage("Cannot log in. Possibly wrong username and/or password!");
        return false;
    }
    
    var results = response[0]; // Get first element of array
    SESSION.ID =  results[0];
    SESSION.USERNAME = results[1].name;
    
	loggedIn();
	setMessage("Have a good day, " + SESSION.USERNAME + "!");
	
	loadContents();
	return true;
}

function processCheckLogin(response)
{
	processLogin(response);
	$('loginGroupbox').hidden = false;
//	$('mainwindowBox').collapsed = false;
	$('loginLoadingBox').hidden = true;
}

function abortLogin()
{
	if(isset(loginHttpRequest))
	{
		loginHttpRequest.abortConnection(30);
		resetLoginForm();
	}
}

function resetLoginForm()
{
	loginHttpRequest = undefined;
	$("loginProgress").mode = "determined";    
	$("loginProgress").hidden = true;
	$("loginUsername").disabled = false;
	$("loginPassword").disabled = false;
	$("loginProfile").disabled = false;
	$("loginButton").label = "Login";
}

function doLogin()
{
	if(isset(loginHttpRequest))
	{
		abortLogin();
		return;
	}
	var username = $('loginUsername').value;
	var password = $('loginPassword').value;
	
	if(CONFIG.DRUPMAN_RUN != 'remote')
		SESSION.URL = $('loginProfile').value;
		
	/* Processing function */
	
	$("loginProgress").mode = "undetermined";
	$("loginProgress").hidden = false;
	$("loginUsername").disabled = true;
	$("loginPassword").disabled = true;
	$("loginProfile").disabled = true;
	$("loginButton").label = "Abort";
	
	/* Preparing for a function call */
	var xmlrpc_call = makeXMLRpcCall("system.login", [username, password]);
	loginHttpRequest = xmlrpc_call.sendRequest(SESSION.URL, processLogin); // Perform asynchronous call.
	// session_id = processLogin(http_req.responseText, http_req.responseXML);
}

function doCheckLogin()
{
	/* Preparing for a function call */
	var xmlrpc_call = makeXMLRpcCall("system.checkLogin", []);
	xmlrpc_call.sendRequest(SESSION.URL, processCheckLogin);
}

function doLogout()
{
	var xmlrpc_logout = makeXMLRpcCall("system.logout",[]);
	xmlrpc_logout.sendRequest(SESSION.URL, loggedOut);
	setMessage("Logging out of the system ...");
}

//-------- Progress bar --------//
function loggedIn()
{
	$("lbLoginId").value = "ID: " + SESSION.USERNAME;
	$("mainDeck").setAttribute("selectedIndex", 1);
}

function loggedOut()
{
	$("lbLoginId").value = "Not logged in.";
	$("mainDeck").setAttribute("selectedIndex",0);
	clearMessage();
}

