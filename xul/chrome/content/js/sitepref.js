function doGetSiteInfo()
{
	/* Get site information */
	function processGetSiteInfo(response)
	{    
	    var results = parseResult(response);     
	    
		$("siteNameText").setAttribute("value", results.site_name);
		$("siteEmailText").setAttribute("value", results.site_mail);
		$("siteSloganText").setAttribute("value", results.site_slogan);
		$("siteMissonText").setAttribute("value", results.site_mission);
		$("siteFooterText").setAttribute("value", results.site_footer);    
	}
	
	/* Preparing for a function call */
	var xmlrpcCall = makeXMLRpcCall("system.getSettingsListAll", []);
	xmlrpcCall.sendRequest(SESSION.URL, processGetSiteInfo); 		
}

function doSetSiteInfo()
{
	/* Preparing for a function call */	
	var content= new Object();	
	content.site_name = $("siteNameText").value;
	content.site_mail = $("siteEmailText").value;
	content.site_slogan = $("siteSloganText").value;
	content.site_mission = $("siteMissonText").value;
	content.site_footer = $("siteFooterText").value;		
	
	var xmlrpcCall = makeXMLRpcCall("system.setSettings", [content]);
	httpRequest = xmlrpcCall.sendRequest(SESSION.URL, processUpdate); 		
}

/* Get all themes of site */
function setImagePreview()
{
	var item = $("listTheme").selectedItem;
	var index = item.value.lastIndexOf("/");	
	var screenshot = CONFIG.SITE_URL + item.value.substring(0, index) + "/screenshot.png";	
	$("previewImg").setAttribute("src", screenshot);	
}


function doGetSiteTheme()
{
	function processGetSiteTheme(response)
	{ 
	    var results = parseResult(response);         
	    
	    var listBox = $("listTheme");        
	    for(var item in results)
	    {	
		var listItem = listBox.appendItem(results[item].name,  results[item].filename);
		if (parseInt(results[item].status) == 1)	
			listBox.selectedItem = listItem;	
	    }    
	}
	
	/* Preparing for a function call */	
	var xmlrpcCall = makeXMLRpcCall("system.getAllThemes", []);
	xmlrpcCall.sendRequest(SESSION.URL, processGetSiteTheme); 		
}

function doSetSiteTheme()
{
	/* Preparing for a function call */	
	var listItem = $("listTheme").selectedItem;        
	var xmlrpcCall = makeXMLRpcCall("system.setSiteTheme", [listItem.label]);
	xmlrpcCall.sendRequest(SESSION.URL, processUpdate); 		
}

/* Get all blocks of site */
function selectBlock()
{	
	var listBox = $("blocksList");	
	if (listBox.selectedIndex < 0)
		{ return; }
	var listItem = listBox.getItemAtIndex(listBox.selectedIndex);		
	if (listItem.value ==1)
	{		
		$("enabledCheckbox").checked = true;
	}
	else
	{		
		$("enabledCheckbox").checked = false;
	}			
	$("blockWeight").selectedItem = $(listItem.getAttribute("weight"));	
	switch (listItem.getAttribute("region"))
	{		
		case "left":
			$("blockPlacement").selectedItem = $("left");
			break;
		case "right":
			$("blockPlacement").selectedItem = $("right");
			break;
		case "content":
			$("blockPlacement").selectedItem = $("content");
			break;
		case "header":
			$("blockPlacement").selectedItem = $("header");
			break;
		case "footer":		
			$("blockPlacement").selectedItem = $("footer");
			break;
	}	
}

function applyBlockInfo()
{
	var listBox = $("blocksList");	
	if (listBox.selectedIndex < 0)
		return;
	var listItem = listBox.getItemAtIndex(listBox.selectedIndex);	
	
	$("enabledCheckbox").checked ? listItem.value = 1 : listItem.value = 0;	
	listItem.childNodes[1].setAttribute('label',$("enabledCheckbox").checked);
	
	listItem.setAttribute("weight", $("blockWeight").selectedItem.value);
	listItem.childNodes[2].setAttribute('label',$("blockWeight").selectedItem.value);
	
	listItem.setAttribute("region", $("blockPlacement").selectedItem.value);		
	listItem.childNodes[3].setAttribute('label', $("blockPlacement").selectedItem.value);
}


function doGetSiteBlock()
{
	function processGetSiteBlock(response)
	{  
	    var results = parseResult(response);         
	    
	    Logger.dump(results,"Blocks list");
	    var listBox = $("blocksList");        
	    for(var item in results)
	    if(typeof results[item] != 'function')
	    {	
			var listItem = $E({
				tag: 'listitem',
				children :
				[{
					tag : 'listcell',
					label : results[item].info
				},{
					tag : 'listcell',
					label :  results[item].status == 1 ? 'true' : false
				},{ 
					tag : 'listcell',
					label :  results[item].weight
				},{ 
					tag : 'listcell',
					label :  results[item].region
				}]
			});		

			listItem.value = results[item].status;
			listItem.setAttribute("weight", results[item].weight);
			listItem.setAttribute("region", results[item].region);	
			listItem.setAttribute("module", results[item].module);	
			listItem.setAttribute("delta", results[item].delta);	

			listBox.appendChild(listItem);
	    }        
	    // select the first item
	    listBox.selectedItem = listBox.childNodes[2];
	}
		
	/* Preparing for a function call */	
	var xmlrpcCall = makeXMLRpcCall("system.getAllBlocks", []);
	httpRequest = xmlrpcCall.sendRequest(SESSION.URL, processGetSiteBlock); 		
}

function doSetSiteBlock()
{
	/* Preparing for a function call */
	var listBox = $("blocksList");
	var content = new Array();
	for(i = 1; i < listBox.childNodes.length; i++)
	{
		var item = new Object();
		item.info = listBox.childNodes[i].childNodes[0].getAttribute("label");
		item.status = listBox.childNodes[i].value;
		item.weight = listBox.childNodes[i].getAttribute("weight");
		item.region = listBox.childNodes[i].getAttribute("region");
		item.module = listBox.childNodes[i].getAttribute("module");
		item.delta = listBox.childNodes[i].getAttribute("delta");
		content.push(item);
	}
	var xmlrpcCall = makeXMLRpcCall("system.setAllBlocks", [content]);
	httpRequest = xmlrpcCall.sendRequest(SESSION.URL, processUpdate); 			
}

function initWeightList()
{
	var blockList = $("blockWeight");
	var menuPopup = document.createElement("menupopup");
	for (i = -10; i <= 10; i++)
	{
		var menuItem = document.createElement("menuitem");
		menuItem.setAttribute("id", i);
		menuItem.setAttribute("label", i);
		menuItem.setAttribute("value", i);		
		menuPopup.appendChild(menuItem);
	}	
	blockList.appendChild(menuPopup);	
}

/* init setting dialog */
function initSitepref()
{
	initWeightList(); 
	doGetSiteInfo();
	doGetSiteTheme(); 
	doGetSiteBlock();
}

/* dialog accept */
function acceptDialog()
{
	/* save settings */
	doSetSiteInfo();
	doSetSiteTheme();
	doSetSiteBlock();
	return true;
}

/* dialog cancel */
function cancelDialog()
{
	return true;
}

function processUpdate(response)
{
	// $("xmlResponse").value = txt;
	var results = parseResult(response.content);
	Logger.dump(results,"Sitepref update results ...");
	return true;
}