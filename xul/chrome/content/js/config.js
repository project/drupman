/**
* CONFIGURATION FILE
*/
function CONFIG_CLASS() {
		/*
		// {09A254C9-1ABA-4fff-A917-899AD20E7C3D}
		static GUID <<name>> = 
		{ 0x9a254c9, 0x1aba, 0x4fff, { 0xa9, 0x17, 0x89, 0x9a, 0xd2, 0xe, 0x7c, 0x3d } };
		*/
		this.GUID = '{09A254C9-1ABA-4fff-A917-899AD20E7C3D}';
		this.DRUPMAN_URL = 'http://lexhung.no-ip.info/drupal472';
		
		this.DRUPMAN_NAME = 'Drupal Manager';
		this.DRUPMAN_VERSION = 'alpha';
		this.DRUPMAN_HOMEPAGE = 'http://www.drupal.org/project/drupman';
		this.DRUPMAN_FEEDBACK_PAGE = 'http://www.drupal.org/project/drupman';
		
		this.MAX_NODES_IN_LIST = 20;
		this.NODES_LIST_PAGE_SIZE = 50;
		
		this.MAX_DUMP_DEPTH = 7;
		this.MAX_DUMP_COUNT = 250;
		this.MAX_LOG_COUNT = 400;
		this.LOG_LEVEL = 0; // 0-5; Message with higher or equals level will be logged.
		this.LOG_XMLRPC = true;
		
		this.VOCABULARY_PREFIX 	= '#voc_';
		this.NODETYPE_PREFIX 	= '#ndt_';
		this.TERM_PREFIX 		= '#trm_';
		this.COMMENT_PREFIX 		= '#cmt_';
		this.RANDOM_PREFIX 		= '#rnd_';
		this.NODE_PREFIX 		= '#nde_';
		
		this.MESSAGE_DISPLAY_TIME = 10;
		
		//local || remote || chrome
		//Local have more permissione than the remote version.
		if((window.location.toString().lastIndexOf('http://',0) == 0) ||
		   (window.location.toString().lastIndexOf('https://',0) == 0)) 
			  this.DRUPMAN_RUN = 'remote';
		else if(window.location.toString().lastIndexOf('chrome://',0) == 0)
			  this.DRUPMAN_RUN = 'chrome';
		else
			  this.DRUPMAN_RUN = 'local';
}

var CONFIG = new CONFIG_CLASS();