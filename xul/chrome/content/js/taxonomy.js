/** Vocabulary **********************************************/
NAVTREE.Vocabulary = function(site,voc)	{
	this.vocabulary = voc;
	this.site = site;
	this.createDOMNode(voc.name, "vocabulary");
	
	this.loadSubItems = function()
	{
		var _self = this;
		function processTermSubtree(results, e)
		{
			for(var i=0; i < results.length; i++)	{
			  var term = new mod.Term(_self.site, results[i]);
		   	XULTREE.addUniqueChild(_self.DOMNode, term.DOMNode);
			}
			_self.removeWaitingState();
		}
		
		this.setWaitingState();
		this.site.service.taxonomy.getTermSubtree(parseInt(this.vocabulary.vid), 0, 1, processTermSubtree);
	}
}
DRUPMAN.extend(NAVTREE.Vocabulary,NAVTREE.NavtreeItem);

/** Term **********************************************/
NAVTREE.Term = function(site,tm)	{
	this.term = tm;
	this.site = site;
	this.createDOMNode(tm.name, "term");
	
	publ.loadSubItems = function()
	{
		var _self = this;
		function processTermSubtree(results)
		{
			var count = 0;
			for(var i=0; i < results.length; i++)	{
			  var term = new mod.Term(_self.site, results[i]);
		   	XULTREE.addUniqueChild(_self.DOMNode, term.DOMNode);
		   	count++;
			}
			
			if(count == 0)
	  		_self.DOMNode.setAttribute('container','false');
			
			_self.removeWaitingState();
		}
		this.setWaitingState();
		this.site.service.taxonomy.getTermSubtree(parseInt(this.term.vid), parseInt(this.term.tid), 1, processTermSubtree);
	}
}
DRUPMAN.extend(NAVTREE.Term,NAVTREE.NavtreeItem);

/** Nodetype **********************************************/
NAVTREE.NodeType = function(site, name)	{
	this.site = site;
  this.nodetype = {};
	this.nodetype.name = name;
  this.createDOMNode(name,	"nodetype");
  this.DOMNode.setAttribute('container','false');
}
DRUPMAN.extend(NAVTREE.NodeType,NAVTREE.NavtreeItem);
	
/** NodeTypesList **********************************************/
NAVTREE.NodeTypesList = function(site)	{
	this.site = site;
  this.createDOMNode("Node Types",	"nodetypes");
  
	this.loadSubItems = function()
	{
	  var _self = this;
		function processNodeTypes(results)
		{
			for(var i=0; i < results.length; i++)	{
			   var term = new mod.NodeType(_self.site, results[i]);
	   		 XULTREE.addChild(_self.DOMNode, term.DOMNode);
			}
	  	_self.removeWaitingState();
		}
		
		this.setWaitingState();
		this.site.service.node.getNodeTypes(processNodeTypes);
	}
}
DRUPMAN.extend(NAVTREE.NodeTypeList,NAVTREE.NavtreeItem);


/** VocabulariesList **********************************************/
NAVTREE.VocabulariesList = function(site)	{
	this.site = site;
  this.createDOMNode("Vocabularies",	"nodetypes");
	
	this.loadSubItems = function()
	{
	  var _self = this;
		function processVocabularies(results)
		{
			for(var _vid in results)
			if(typeof results[_vid] != 'function')	{
			   var voc = new mod.Vocabulary(_self.site, results[_vid]);
	   		 XULTREE.addChild(_self.DOMNode, voc.DOMNode);
			}
			
			_self.removeWaitingState();
		}
		_self.setWaitingState();
		this.site.service.taxonomy.getVocabularies(processVocabularies);
	}
}
DRUPMAN.extend(NAVTREE.VocabularyList,NAVTREE.NavtreeItem);

/** Site **********************************************/
NAVTREE.Site = function(site)	{
	this.site = site;
  this.createDOMNode(site.sitename,	"site");
	  
	this.loadSubItems = function()
	{
		var nodetypes = new mod.NodeTypesList(this.site);
		var vocabularies = new mod.VocabulariesList(this.site);
 		XULTREE.addChild(this.DOMNode, nodetypes.DOMNode);
 		XULTREE.addChild(this.DOMNode, vocabularies.DOMNode);
	}
}
DRUPMAN.extend(NAVTREE.Site,NAVTREE.NavtreeItem);
