/**
* Class for managing sites, each site is an instance of this class.
*/
var NAVTREE = {
}

NAVTREE.NavtreeItem = function()	{
  this.DOMNode = null;
}

NAVTREE.NavtreeItem.prototype.createDOMNode = function(nodeLabel, props)
{
  if(!isset(this.DOMNode))
  {
		this.DOMNode = $E({
			tag : 'treeitem',
			container : true,
			children : {
				tag : 'treerow',
				children : {
					tag : 'treecell',
					properties : props,
					label : nodeLabel
				}
			}								 
		});
		
		this.DOMNode.navtreeObject = this;
	}
	return this.DOMNode;
}
	
NAVTREE.NavtreeItem.prototype.setWaitingState = function()
{
	var cell = this.DOMNode.firstChild.firstChild;
	this.cellprops = cell.getAttribute('properties');
	cell.setAttribute('properties','loading');
}

NAVTREE.NavtreeItem.prototype.removeWaitingState = function()
{
	var cell = this.DOMNode.firstChild.firstChild;
	if(isset(this.cellprops))
		cell.setAttribute('properties',this.cellprops);
}
	
NAVTREE.NavtreeItem.prototype.loadSubItems = function() {}

	
/** Site **********************************************/
NAVTREE.Site = function(site)	{
	this.site = site;
  this.createDOMNode(site.sitename,	"site");
	
	this.loadSubItems = function()
	{
		var themeselector = new NAVTREE.ThemeSelector(this.site);
		var nodemanager = new NAVTREE.NodeManager(this.site);
			
 		XULTREE.addChild(this.DOMNode, nodemanager.DOMNode);
 		XULTREE.addChild(this.DOMNode, themeselector.DOMNode);
	}
}
DRUPMAN.extend(NAVTREE.Site, NAVTREE.NavtreeItem);

/** Drupman - Main navtree item **********************************************/
NAVTREE.Drupman = function()	{
  this.createDOMNode("Drupal Manager",	"drupman");
		
	this.loadSubItems = function()
	{
		var sitesmanager = new NAVTREE.SitesManager();
		var logviewer = new NAVTREE.LogViewer();
		var preftree = new NAVTREE.PreferenceTree();
			
 		XULTREE.addChild(this.DOMNode, sitesmanager.DOMNode);
 		XULTREE.addChild(this.DOMNode, preftree.DOMNode);
 		XULTREE.addChild(this.DOMNode, logviewer.DOMNode);
	}
		
	this.activate = function() {
		switchMainView("overview");
	}
}
DRUPMAN.extend(NAVTREE.Drupman, NAVTREE.NavtreeItem);
	
/** Link for NodeManager **********************************************/
NAVTREE.NodeManager = function(site)	{
  this.createDOMNode("Node Manager",	"node-manager");
  this.DOMNode.removeAttribute("container");
  this.site = site;

	this.activate = function() {
		switchMainView("nodeManagerPanel");
		if(isset(this.site))
		{
			$("nodeManagerTab").setAttribute("label",this.site.sitename);
			$("nodeManagerTabpanel").activeSID = this.site.sid;
			$("nodeManagerTabpanel").nodeList.queryObject = null;
			$('TaxonomyTree').activeSID = this.site.sid;
		}
	}
}
DRUPMAN.extend(NAVTREE.NodeManager, NAVTREE.NavtreeItem);

/** Link for Log Viewer tree item**********************************************/
NAVTREE.LogViewer = function()	{
	  this.createDOMNode("Log Viewer",	"log-viewer");
	  this.DOMNode.removeAttribute("container");
	
		this.activate = function() {
			switchMainView("debugConsolePanel");
		}
}
DRUPMAN.extend(NAVTREE.LogViewer, NAVTREE.NavtreeItem);

	/** Link for Theme Selector **********************************************/
NAVTREE.ThemeSelector = function(site)	{
  this.createDOMNode("Theme Selector",	"theme-selector");
  this.DOMNode.removeAttribute("container");
  this.site = site;
  
	this.activate = function() {
		switchMainView("themeSelectorPanel");
		if(site.getStatus() == 'logged_in')
		{
			$("themeSelector").activeSID = this.site.sid;
		}
	}
}
DRUPMAN.extend(NAVTREE.ThemeSelector, NAVTREE.NavtreeItem);
	
/** Link for Sites Manager **********************************************/
NAVTREE.SitesManager = function()	{
  this.createDOMNode("Sites Manager",	"sites-manager");
  
	this.activate = function() {
		switchMainView("siteManagerPanel");
	}

	this.loadSubItems = function()
	{
		for(i in DRUPMAN.SiteList)
		if(typeof DRUPMAN.SiteList[i] != 'function')
		{
			var s = new NAVTREE.Site(DRUPMAN.SiteList[i]);
	  	s.DOMNode.removeAttribute("container");
			XULTREE.addChild(this.DOMNode, s.DOMNode);
		}
	}
}
DRUPMAN.extend(NAVTREE.SitesManager, NAVTREE.NavtreeItem);

	
	/** Link for Sites Manager **********************************************/
NAVTREE.PreferenceTree = function()	{
  this.createDOMNode("Preference Tree",	"preference-tree");
  this.DOMNode.removeAttribute("container");
	
	this.activate = function() {
		switchMainView("preferencePanel");
	}
}
DRUPMAN.extend(NAVTREE.PreferenceTree, NAVTREE.NavtreeItem);

