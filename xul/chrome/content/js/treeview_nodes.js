const _VOCABULARY_PREFIX = '#voc_';
const _NODETYPE_PREFIX = '#ndt_';
const _TERM_PREFIX = '#trm_';

NodesTreeView.prototype = new TreeView();
NodesTreeView.prototype.constructor = NodesTreeView;
NodesTreeView.prototype.baseClass = TreeView.prototype.constructor;

function NodesTreeView(domTree)
{
	this.childData = {
		rootNode : {
	    	name : 'Node types',
	    	hasChild : true,
	    	child : [],
	    },
		rootTaxonomy : {
	    	name : 'Taxonomies',
	    	hasChild : true,
	    	child : [],
	    },
	    _nodeLoading : {
	    	name : 'Loading child nodes ...',
	    	hasChild : false,
	    	imageSrc : '../skin/icons/animated/spin-ff16.gif',
	    },
	 }
	
	// Node ID > Node Level > isContainerOpen > Temp ID
	this.visibleData = [
	    ['rootNode'		, 0, false],
	    ['rootTaxonomy'	, 0, false]
	];
	
	this.columnIdMap = {
		colNodeTypes : "name",
	}
	this.__proto__.DOMTree = domTree;
	// this.validateChildData();

}

NodesTreeView.prototype.loadSubItems = function(item_idx) {
	var loadingID = this.getRandomID();
	var item_id = this.visibleData[item_idx][_TREEVIEW_ID];
	
	this.addTemporaryChild(item_idx,'_nodeLoading',loadingID);
	
	if(item_id == 'rootNode')
	{
		/// ddump(item_id,"ITEMS ID");
		this.loadNodeTypes(item_id, loadingID);
	}
	else if(item_id == 'rootTaxonomy')
	{
		this.loadVocabularies(item_id, loadingID);
	}
	else
	{
		this.loadTermSubtree(item_id, loadingID);
	}

}

NodesTreeView.prototype.loadNodeTypes = function(parent_id, loadingID)
{
	var me = this;
	function processNodeTypes(response)
	{
		var results = parseResult(response);
				
		for(var i=0; i< results.length; i++)
		{
			var id = _NODETYPE_PREFIX + results[i];
			var new_item = new Object();
			new_item.hasChild = false;
			new_item.name = results[i];
			me.childData[id] = new_item;
			me.childData[parent_id].child.push(id);
		}
		me.removeTemporaryChild(loadingID);
	}
	
	var xmlrpc_getnodetypes = makeXMLRpcCall("node.getNodeTypes", []);
	xmlrpc_getnodetypes.sendRequest(SESSION.URL, processNodeTypes);
}

NodesTreeView.prototype.loadVocabularies = function(parent_id, loadingID)
{
	var me = this;
	function processVocabularies(response)
	{
		var results = parseResult(response);
				
		for(var vid in results)
		{
			var id = _VOCABULARY_PREFIX + vid;
			results[vid].hasChild = true;
			me.childData[id] = results[vid];
			me.childData[parent_id].child.push(id);
		}
		me.removeTemporaryChild(loadingID);
	}
	
	var xmlrpc_getnodetypes = makeXMLRpcCall("node.getVocabularies", []);
	xmlrpc_getnodetypes.sendRequest(SESSION.URL, processVocabularies);
}

NodesTreeView.prototype.loadTermSubtree = function(parent_id, loadingID)
{
	var me = this;
	function processTermSubtree(response)
	{
		var results = parseResult(response);
		
		ddump(results);
		for(i=0; i< results.length; i++)
		{
			var id = _TERM_PREFIX + results[i].tid;
			results[i].hasChild = true;
			me.childData[id] = results[i];
		}
		
		// TWO SEPARATE LOOP
		for(i=0; i < results.length; i++)
			for(j=0;j < results[i].parents.length;j++)
			{
				if(results[i].parents[j] == 0)
					_pid = _VOCABULARY_PREFIX + results[i].vid;
				else
					_pid = _TERM_PREFIX + results[i].parents[j];
					
				if(me.childData[_pid].child == undefined)
					me.childData[_pid].child = [];
				me.childData[_pid].child.push(_TERM_PREFIX + results[i].tid);
			}
		
		me.removeTemporaryChild(loadingID);
		
		if(results.length == 0)
			me.childData[parent_id].hasChild = false;				
	}

	var vid = this.childData[parent_id].vid;
	if(parent_id.indexOf(_VOCABULARY_PREFIX,0) == 0) {	
		xmlrpc_gettermsubtree = makeXMLRpcCall("node.getTermSubtree", [parseInt(vid),0,30]);
	}
	else if(parent_id.indexOf(_TERM_PREFIX,0) == 0)	{
		var tid = this.childData[parent_id].tid;
		xmlrpc_gettermsubtree = makeXMLRpcCall("node.getTermSubtree", [parseInt(vid),parseInt(tid),30]);
	}
	xmlrpc_gettermsubtree.sendRequest(SESSION.URL,processTermSubtree);
}

/*	
function NodesManager()
{

}
function treeNodeTypesSelect(tree)
{
	var idx = tree.currentIndex;
	var treeitem = tree.contentView.getItemAtIndex(idx);
	var treerow = treeitem.firstChild;
	alert(treerow.firstChild.getAttribute("properties",prop));	
}

NodesManager.prototype.loadNodeTypes = function(root) {
}

NodesManager.prototype.loadTaxonomy = function(root)
{
}
*/