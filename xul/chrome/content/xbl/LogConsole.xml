<?xml version="1.0" encoding="UTF-8"?>

<bindings id="LogConsoleBindings"
	xmlns="http://www.mozilla.org/xbl"
	xmlns:xbl="http://www.mozilla.org/xbl"
	xmlns:xul="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">
	<!--
		This object provides a log function which performs loggin tasks.
		It takes following parameters :
			message : log message
			level   : log level
			extra_object : extra object to be dumped
			... : additional parameters to be added after the message
				  for further information. 
			type 	: log type (?) A field of log_type should be added
			for compatibility with Drupal's version.
	-->
	<binding id="LogConsole">
		<content>
			<xul:vbox flex="1">
				<xul:hbox anonid="logController" align ="center">
					<xul:label value="Log Level:" />
					<xul:menulist id="_log_level" oncommand="document.getBindingParent(this).level = this.value;">
						<xul:menupopup>
							<xul:menuitem label="Verbose"	value="0" />
							<xul:menuitem label="Debug"		value="1" />
							<xul:menuitem label="Info"		value="2" />
							<xul:menuitem label="Warning"	value="3" />
							<xul:menuitem label="Error"		value="4" />
							<xul:menuitem label="Fatal" 	value="5" />
						</xul:menupopup>
					</xul:menulist>
					<xul:label value="Show :" />
					<xul:menulist anonid="mlViewLevel" oncommand="document.getBindingParent(this).viewlevel = this.value;">
						<xul:menupopup>
							<xul:menuitem label="Verbose"	value="0" />
							<xul:menuitem label="Debug"		value="1" />
							<xul:menuitem label="Info"		value="2" />
							<xul:menuitem label="Warning"	value="3" />
							<xul:menuitem label="Error"		value="4" />
							<xul:menuitem label="Fatal" 	value="5" />
						</xul:menupopup>
					</xul:menulist>
					<xul:checkbox anonid="cbViewHigher" label="Show higher level(s)" checked="true" oncommand="document.getBindingParent(this).updateLogList();"/>
					<xul:button label="Clear" oncommand = "document.getBindingParent(this).clearLog();"/>
				</xul:hbox>
				<xul:richlistbox anonid="logList" class="_logger" flex="1" />
			</xul:vbox>
		</content>
		
		<resources>
			<stylesheet src="../../skin/logger.css"/>
		</resources>
		
		<implementation>
			<property name = "VERBOSE" 	readonly="true" 	onget = "return 0;" />
			<property name = "DEBUG" 	readonly="true" 	onget = "return 1;" />
			<property name = "INFO" 	readonly="true" 	onget = "return 2;" />
			<property name = "WARNING" 	readonly="true" 	onget = "return 3;" />
			<property name = "ERROR" 	readonly="true" 	onget = "return 4;" />
			<property name = "FATAL" 	readonly="true" 	onget = "return 5;" />
			<constructor>
				<![CDATA[]]>
			</constructor>
			
			<property name = "level" >
				<getter><![CDATA[
					return CONFIG.LOG_LEVEL;
				]]></getter>
				<setter><![CDATA[
					var n = parseInt(val);
					if(!isNaN(n) && n >= this.VERBOSE && n <= this.FATAL)
					{
						this._mlLogLevel.value = CONFIG.LOG_LEVEL = n;
						this.log("Log level set to : " + this.CSS_CLASSES[n],n);
					}
					else
					{
						this.verbose("Cannot set log level to : " + val);
					}
					return val;
				]]></setter>
			</property>
			
			<property name = "viewlevel" >
				<getter><![CDATA[
					return this._viewLevel;
				]]></getter>
				<setter><![CDATA[
					var n = parseInt(val);
					if(!isNaN(n) && n >= this.VERBOSE && n <= this.FATAL)
					{
						this._mlViewLevel.value = n;
						this.updateLogList();						
					}
					else
					{
						this.verbose("Cannot set view level to : " + val);
					}
					return val;
				]]></setter>
			</property>
						
			<constructor><![CDATA[
				this.CSS_CLASSES = ['verbose', 'debug','info','warning','error','fatal'];
				
				this._controller = document.getAnonymousElementByAttribute(this,'anonid','logController');;
				this._logList = document.getAnonymousElementByAttribute(this,'anonid','logList');;
				this._mlLogLevel   = _controller.childNodes[1];
				this._mlViewLevel  = _controller.childNodes[3];
				this._cbViewHigher = _controller.childNodes[4];
				
				this._mlViewLevel.value = this._viewLevel = this.DEBUG;
				this._mlLogLevel.value = CONFIG.LOG_LEVEL;
				
				var _self = this;
				function windowError(message, url, line) {
					_self.log('Error on line ' + line + ' of document ' + url + ' : ' + message, 5);
					return true; //
				}
				
				// only override the window's error handler if logging is turned on
				// window.onerror = windowError;
				
			]]></constructor>
			
			<method name="log">
				<parameter name="log_message" />
				<parameter name="log_level" />
				<parameter name="extra_object" />
				<body><![CDATA[
					if(this.getAttribute('disabled') == true)
						return false;
					
					if(arguments.length < 2) { 
						log_level = this.INFO;
					}
						
					if(log_level < CONFIG.LOG_LEVEL || log_level > this.FATAL)
						return false;
					
					var new_log_item = {
						tag : 'richlistitem',
						item_level : log_level,
						class : this.CSS_CLASSES[log_level],
						children : {
							tag : 'vbox',
							flex : '1',
							children : [{
								tag : 'hbox',
								align : 'center',
								flex : '1',
								children : [{
									tag : 'description',
									class : 'log-title',
									flex : '1',
									children : this.CSS_CLASSES[log_level].toUpperCase() + ' : ' + log_message,
								}, { 
									tag : 'spacer', flex : '1' 
								}]	
							}]
						}
					};
					
					for(var i=4; i < arguments.length; i++)
						new_log_item.children.children[0].children.push({
									tag : 'label',
									value : arguments[i] + "   || "
								});
								
					if(isset(extra_object))
					{
						new_log_item.ondblclick = "$('" + this.id + "').toggleDetails();";
						new_log_item.children.children[0].children.push({
									tag : 'label',
									class : 'text-link',
									onclick : "$('" + this.id + "').toggleDetails();",
									value : 'Details'
								});
						
						if(typeof extra_object.length != 'undefined' && extra_object.length == 0)
							object_title = "<Empty Array>";
						else
							object_title = (typeof extra_object).toUpperCase();
						
						var dom_object = this.dumpObject(extra_object, object_title);
						dom_object.setAttribute("hidden","true");
						new_log_item.children.children.push(dom_object);
					}
					
					// Hide log entries that don't match view level
					if((log_level == this._mlViewLevel.value) ||
					  ((log_level > this._mlViewLevel.value)  && this._cbViewHigher.checked))
						new_log_item.hidden = "false";
					else
						new_log_item.hidden = "true";
					
					// Add log entry to log list
					this._logList.insertBefore($E(new_log_item),this._logList.firstChild);
					
					// Remove last entry if the log is too long
					if(this._logList.getRowCount() > CONFIG.MAX_LOG_COUNT)
						this._logList.removeChild(this._logList.lastChild);
					
					return true;
				]]></body>
			</method>

			<method name="updateLogList">
				<body><![CDATA[
					var list = this._logList.getElementsByTagName("richlistitem");
					var level = this._mlViewLevel.value;
					var higher = this._cbViewHigher.checked;
					for(var i=0;i < list.length; i++)
					{
						il = list[i].getAttribute("item_level");
						if((il == level) || ((il > level) && higher))
							list[i].setAttribute("hidden","false");
						else
							list[i].setAttribute("hidden","true");
					}
				]]></body>
			</method>
			
			<method name="dumpObject">
				<parameter name="obj" />
				<parameter name="name" />
				<body><![CDATA[
				    var LABEL_WIDTH = 70;
					function dumpObjectAsText(obj, name, indent, depth) {
					    if (depth > MAX_DUMP_DEPTH) {
					        return indent + name + ": <Maximum Depth Reached>\n";
					    }
					    if (typeof obj == "object") {
					        var child = null;
					        var output = indent + name + "\n";
					        indent += "\t";
					        for (var item in obj) {
					            try {
					                child = obj[item];
					            }
					            catch (e) {
					                child = "<Unable to Evaluate>";
					            }
					            if (typeof child == "object") {
					                output += dumpObjectAsText(child, item, indent, depth + 1);
					            } else if (typeof child != 'function') {
					            	output += indent + item + "\t: " + child + "\n";
					            }
					        }
					        return output;
					    } else {
					        return obj;
					    }
					}
									
					function dumpObjectAsDom(obj, name, depth) {
					    if (depth > CONFIG.MAX_DUMP_DEPTH)
					        return $E({
					        	tag : 'label', 
					        	value : name + ": <Maximum Depth (" + CONFIG.MAX_DUMP_DEPTH + ") Reached! >\n" });
					        		
					    if((typeof obj == "object") && isset(obj) && !isset(obj.ownerDocument)) {
					        var child = null;
					        var output = null;
					        
							// If obj is an object, create a hbox with
							// the label and a vbox for child items						
				        	output = $E({
				        		tag : 'hbox',
				        		flex : '1',
				        		children : [{
				        			tag : 'label',
				        			class : 'log-object-key',
				        			onclick : 'toggleHidden(this.nextSibling);',
				        			crop : 'right',
				        			width : LABEL_WIDTH,
				        			value : name + " :",
				        		},
				        		{
				        			tag : 'vbox',
				        			flex : '1'
				        		}]
				        	});
				        	
							var output_vbox = output.childNodes[1];
							var exception_caught = false;
							var loop_count = -1;
												        
					        for (var item in obj) 
				        	if(typeof obj[item] != 'function')
					        {
					            try {
					                child = obj[item];
					            }
					            catch(e) {
					            	// An exception caught, leave a message an continue
					            	exception_caught = true;
					                child = $E({  	
				                		tag : 'description',
							        	flex : 1, 
							        	children : child + " : <Unable to Evaluate> : " + e });
								    output_vbox.appendChild(child);
							        continue;
					            }
					            
					        	loop_count++;
					        	
					        	// Too many sub items, break.
					        	if(loop_count > CONFIG.MAX_DUMP_COUNT)
					        	{
					                child = $E({ tag : 'description',
								        	flex : 1, 
								        	children : "<MAX_DUMP_COUNT(" + CONFIG.MAX_DUMP_COUNT + ") reached!>"});
								    output_vbox.appendChild(child);
								    break;
					        	}
					            
					            if(!exception_caught)
					                output_vbox.appendChild(dumpObjectAsDom(child, item, depth + 1));
					        }
					        
					        if(loop_count < 0)
					        	output_vbox.appendChild($E({tag : 'label', value :'<emtpy object>'}));
					        		
					        return output;
					    } else {
							// If 'obj' is not a object, create a hbox with
							// the label and another label with obj string value
							var val = isset(obj) ? obj : "<value not set>";
								
							try {
								if(isset(obj.ownerDocument))
									val = "<DOM Element>";
							} catch(e) {}
													
				        	return $E({
				        		tag : 'hbox',
				        		flex : '1',
				        		children : [{
				        			tag : 'label',
				        			value : name + ": ",
				        			crop : 'right',
				        			width : LABEL_WIDTH,
				        		},
				        		{
				        			tag : 'description',
				        			flex : '1',
				        			children : val
				        		}]
				        	});
					    }
					}
					
					return dumpObjectAsDom(obj,name,0);
				]]>
				</body>
			</method>

			<method name="clearLog">
				<body><![CDATA[
					while(this._logList.childNodes.length > 0)
						this._logList.removeChild(this._logList.lastChild);
				]]></body>
			</method>

			<method name="toggleDetails">
				<body><![CDATA[
					var item = this._logList.selectedItem;
					if(isset(item))
						toggleHidden(item.firstChild.lastChild);											
				]]></body>
			</method>
			
			<method name="verbose">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
				{
					if(arguments.length > 1)
						return this.log(message, this.VERBOSE, extra_obj);
					return this.log(message,this.VERBOSE);
				}
				]]></body>
			</method>
			<method name="debug">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
					if(arguments.length > 1)
						return this.log(message,this.DEBUG,extra_obj);
					return this.log(message,this.DEBUG);
				]]></body>
			</method>
			<method name="dump">
				<parameter name="obj" />
				<parameter name="name" />
				<body><![CDATA[
					return this.log("Dumping object : " + name, this.DEBUG,obj);
				]]></body>
			</method>			
			<method name="info">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
					if(arguments.length > 1)
						return this.log(message,this.INFO,extra_obj);
					return this.log(message,this.INFO);
				]]></body>
			</method>
			<method name="warning">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
					if(arguments.length > 1)
						return this.log(message,this.WARNING,extra_obj);
					return this.log(message,this.WARNING);
				]]></body>
			</method>
			<method name="error">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
					if(arguments.length > 1)
						return this.log(message,this.ERROR,extra_obj);
					return this.log(message,this.ERROR);
				]]></body>
			</method>
			<method name="fatal">
				<parameter name="message" />
				<parameter name="extra_obj" />				
				<body><![CDATA[
					if(arguments.length > 1)
						return this.log(message,this.FATAL,extra_obj);
					return this.log(message,this.FATAL);
				]]></body>
			</method>
		</implementation>
	</binding>
</bindings>		