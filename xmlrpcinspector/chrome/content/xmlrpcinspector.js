/* ***** BEGIN LICENSE BLOCK *****
 *   Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Drupal Manager.
 *
 * The Initial Developer of the Original Code is
 * Le Xuan Hung.
 * Portions created by the Initial Developer are Copyright (C) 2006
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 * 
 * ***** END LICENSE BLOCK ***** */

var XMLRPCINSPECTOR = {
  onLoad: function() {
    // initialization code
    if(!this.initialized)
    {
	    this.initialized = true;
	    
	    if(!DRUPMAN)
	    {
	    	alert("Drupal Manager has not installed! Please install Drupal Manager before installing Drupman XMLRPC Inspector");
	    	return;
	    }
	    this.strings = document.getElementById("drupman-strings");
	  }
  },
  
  activate : function () {
  	switchMainView('xmlrpcinspector');
  },
  
  /**
   * This method should be called outside XMLRPCINSPECTOR context. Do not use 'this'. 
   */
  onActiveSiteChanged : function() {
  	var site = DRUPMAN.getActiveSite();
  	var treeRoot = $('xmlrpcMethodTreeRoot');
  	
  	if(!isset(site))
  	{
  		setMessage("XMLRPCINSPECTOR.onActiveSiteChanged : No active site.");
  		return;
  	}
  	
  	function processListMethods(data, exception) {
			treeRoot.firstChild.firstChild.setAttribute('properties','site');
  		if(isset(exception))
  		{
  			Logger.debug("XMLRPCINSPECTOR.onActiveSiteChanged : Exception caught!",exception);
  			return;
  		}
  		
  		XMLRPCINSPECTOR.buildMethodTree(site.sitename,data);
  		treeRoot.setAttribute('open',true);
  	}
  	
  	if(site.getStatus() == 'unavailable')
  	{
	  	XULTREE.clearTreeContents(treeRoot);
	  	treeRoot.firstChild.firstChild.setAttribute('label',"\"" + site.name + "\" is unavailable");
  	}
  	else
  	{
  		treeRoot.firstChild.firstChild.setAttribute('properties','loading');
  		site.service.system.listMethods(processListMethods);
  	}
  },
  
  buildMethodTree : function(sitename, methodList) {
  	var currentElement;
  	var treeRoot = $('xmlrpcMethodTreeRoot');
  	XULTREE.clearTreeContents(treeRoot);
  	treeRoot.firstChild.firstChild.setAttribute('label',sitename);

  	for(var i=0;i < methodList.length;i++)
  	{
  		var method = methodList[i].split('.');
  		var packageID = "";
  		currentElement = treeRoot;
  		
  		for(var j=0; j < method.length; j++)
  		{
  		  packageID += method[j];
  			if(j == method.length - 1)
  			{
  				XULTREE.addChild(currentElement, this.constructTreeItem(method[j], methodList[i],'method'));
  				break;
  			}
  			
  			// Search for packages
  			parentElement = currentElement;
  			currentElement = XULTREE.getTreechildren(currentElement).firstChild;
  			while(currentElement && (currentElement.id != packageID))
  				currentElement = currentElement.nextSibling;
  			
  			// Create package node if not exist
  			if(!isset(currentElement))
  				currentElement = XULTREE.addChild(parentElement, this.constructTreeItem(method[j], packageID, 'package'));
  		}
  	}
  },
  
  constructTreeItem : function(label, id, type)
  {
  	var treeitem = $E({
  		tag : 'treeitem',
  		id : id,
  		type : type,
  		children : [{
  			tag : 'treerow',
  			children : [{
  				tag : 'treecell',
		  		properties : type,
  				label : label,
  			}, {
  				tag : 'treecell',
  				label : id,
  			}]
  		}, {
  			tag : 'treechildren'
  		}]
  	});
  	
  	return treeitem;
  },
  
  queryServerCapabilities : function() {
  	var site = DRUPMAN.getActiveSite();
  	if(isset(site))
	  	site.service.system.getCapabilities(function(data) {
		  	$('xmlrpcOutput').value = "Server \"" + site.sitename + "\" capabilities : \n" + dumpText(data);
	  	});
  },
  
  queryMethodDetails : function(methodName) {
  	var site = DRUPMAN.getActiveSite();
 	
  	$('xmlrpcMethodName').value = methodName;
  	$('xmlrpcMethodSignature').value = $('xmlrpcMethodDesciption').value = "[Querying ...]";
  	
  	// Query method description
  	site.service.system.methodHelp(methodName, function(data, exception) {
	  	$('xmlrpcMethodDesciption').value = "" + (exception || data);
  	});
  	
  	// Query method signature
  	site.service.system.methodSignature(methodName, function(data, exception) {
	  	$('xmlrpcMethodSignature').value = "" + (exception || data);
  	});
  },
  
  onMethodTreeSelect : function() {
  	var tree = $('xmlrpcMethodTree');
  	var item = tree.contentView.getItemAtIndex(tree.currentIndex);
  	
  	if(item.getAttribute('type') == 'site')
  		this.queryServerCapabilities();
  	else
  		if(item.getAttribute('type') == 'method')
  			this.queryMethodDetails(item.id);
  },
  
  inspectMethod : function(methodName, params) {
  	var site = DRUPMAN.getActiveSite();
  	var httpRequest;
  	
  	function processMethodCall(data, exception) {
  		$('xmlrpcOutput').value = exception || dumpText(data);
  		if($('xmlrpcLogToConsole').checked)
  		{
  			Logger.info("Inspect xmlrpc method \"" + methodName + "\"",
  				{'input' : httpRequest.data,
  				 'result' : exception || data	}
  			);
  		}
  	  $('xmlrpcReceivedXml').setAttribute('src', "data:text/xml," + httpRequest.responseText);
  	}
  	
  	params.push(processMethodCall);
  	httpRequest = eval('site.service.' + methodName + '.apply(this,params)');
  	$('xmlrpcSentXml').setAttribute('src',"data:text/xml," + httpRequest.data);
  },
  
  inspectCurrentMethod : function() {
  	var methodName = $('xmlrpcMethodName').value;
  	var params = $('xmlrpcInput').value;
  	$('xmlrpcOutput').value = '[Querying...]';
  	
  	
  	try {
  		var paramArray = eval(params);
  	} catch(e) {
  		paramArray = "";
  	}
  	
  	if(!isset(paramArray))
  		paramArray = [];
  		
  	if(!(paramArray instanceof Array))
  	{
  		alert('Input parameters must be able to evaluate to an valid array!');
  		return;
  	}
  	
  	if(isset_string(methodName))
  	{
  		if(isset_string(params)) $('xmlrpcCallHistory').appendItem(methodName, params, params);
  		this.inspectMethod(methodName, paramArray);
  	}
  }
};

window.addEventListener("load", function(e) {
	XMLRPCINSPECTOR.onLoad(e);
}, false);
