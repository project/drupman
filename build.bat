@echo off
echo Build script for Drupman on Windows.
echo You need 7z.exe to be on your PATH variable in order to execute this script.
echo The build script will use your TEMP folder at %TEMP%
pause

set x=drupman
set DRUPMAN_DIR=%cd%
xcopy xul %TEMP%\_dmbuild /i /e /y

REM Make the .jar file, containing all the chrome folder.
cd /d %TEMP%\_dmbuild\chrome
7z a -tzip "%x%.jar" * -x!*CVS* -r -mx=0

REM Go back to .\_dmbuild directory
cd ..

REM Remove the chrome folder's content. Exclude the created .jar
rmdir /s /q chrome\content
rmdir /s /q chrome\locale
rmdir /s /q chrome\skin

REM Pack the rest to an .xpi
7z a -tzip "%x%.xpi" *  -x!*CVS* -r -mx=9
cd ..

REM Move the xpi to the base folder
move _dmbuild\%x%.xpi "%DRUPMAN_DIR%\%x%.xpi"

rmdir /s /q _dmbuild

cd /d "%DRUPMAN_DIR%"
pause